<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\admin\UsersController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/posts', [PostController::class, 'index'])->name('posts.index');



Route::match(['get', 'post'], '/admin', [UsersController::class, 'login'])->name('login');
Route::match(['get', 'post'], '/admin/login', [UsersController::class, 'login'])->name('login');
Route::middleware(['auth'])->group(function () {
    Route::match(['get', 'post'], '/admin/dashboard', [UsersController::class, 'dashboard'])->name('dashboard');
    Route::match(['get', 'post'], '/admin/users/index', [UsersController::class, 'index']);
    Route::match(['get', 'post'], '/admin/users/ajax_add', [UsersController::class, 'ajax_add']);
    Route::match(['get', 'post'], '/admin/users/save', [UsersController::class, 'save']);
    Route::match(['get', 'post'], '/admin/users/check_unique_email', [UsersController::class, 'check_unique_email']);
    Route::match(['get', 'post'], '/admin/users/check_unique_username', [UsersController::class, 'check_unique_username']);
    Route::match(['get', 'post'], '/admin/users/ajax_view', [UsersController::class, 'ajax_view']);
    Route::match(['get', 'post'], '/admin/users/ajax_edit', [UsersController::class, 'ajax_edit']);
    Route::match(['get', 'post'], '/admin/users/update/{id}', [UsersController::class, 'update']);
    Route::match(['get', 'post'], '/admin/users/delete/{id}', [UsersController::class, 'delete']);
    Route::match(['get', 'post'], '/admin/users/change_password/{id}', [UsersController::class, 'change_password']);
    Route::match(['get', 'post'], '/admin/users/ajax_change_status', [UsersController::class, 'ajax_change_status']);
    Route::match(['get', 'post'], '/admin/users/profile', [UsersController::class, 'profile']);
    Route::match(['get', 'post'], '/admin/logout', [UsersController::class, 'logout']);
});


Route::get('/', function () {
    return view('welcome');
});
