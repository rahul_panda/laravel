<?php

namespace App\Http\Controllers\admin;
use Hash;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
class UsersController extends Controller
{
    //
    function login(Request $request){
    	if ($request->isMethod('post')) {
             
    		 $credentials = $request->validate([
	            'username' => 'required',
	            'password' => 'required',
        	]);
		  	if (Auth::attempt($credentials,false)) {
		  		$request->session()->regenerate();
	        	return redirect('/admin/dashboard')->withSuccess('You have successfully logged in!');
		  	}
		  	return back()->withErrors([
            	'email' => 'Your provided credentials do not match in our records.',
        	])->onlyInput('username');

            // echo "<pre>";
            //  print_r($request->post());
            //  exit;

			  // echo Hash::make('123456'); exit;
    		//dd(Auth::attempt($credentials));
		}
        
       // echo Hash::make('123456'); exit;
        return view('admin.users.login');
    }

    public function dashboard(){
        
        $breadcrumb = array(["name" => "Dashboard",'URL'=>'dashboard'],['name'=>"Users","URL"=>""]);
       	$this->viewVars['breadcrumb'] = $breadcrumb;
       	$this->viewVars['title'] = 'Users List';// Capitalize the first letter
        //$this->viewVars['role'] = Role::where('status',1)->where('deleted_flag',0)->get()->pluck('role_name','id')->toArray();
		$this->viewVars['users'] = User::where('deleted_flag',0)->orderBy('created_at','desc')->paginate(PAGE_LIMIT);
        return view('admin.users.dashboard',$this->viewVars);
    }

    public function logout(){       
        Auth::logout();       
        Session::flush();       
        return redirect('admin/login');
      }

      function index(){
		$breadcrumb = array(["name" => "Dashboard",'URL'=>'dashboard'],['name'=>"Users","URL"=>""]);
       	$this->viewVars['breadcrumb'] = $breadcrumb;
       	$this->viewVars['title'] = 'Users List';// Capitalize the first letter
           $this->viewVars['role'] = Role::where('status',1)->where('deleted_flag',0)->get()->pluck('role_name','id')->toArray();
		$this->viewVars['users'] = User::where('deleted_flag',0)->orderBy('created_at','desc')->paginate(PAGE_LIMIT);
		// dd($this->viewVars);
		return view('users.index',$this->viewVars);
	}

	function profile(){
		return view('admin.users.profile');
	}
	function ajax_add (){
		//$this->viewVars['roles'] = Role::where('status',1)->where('deleted_flag',0)->get()->pluck('role_name','id')->toArray();
		$this->viewVars['ra_users'] = User::where('status',1)->get()->pluck('name','id')->toArray();
		return view('admin.users.ajax.add',$this->viewVars);
		// echo 1; exit;
	}

	function save(Request $request){
		if($request->isMethod('post')){
			$postData = $request->all();
          
			$request->validate([
	            'name'=> 'required',
            	'email'=> 'required|unique:users|max:255',            	
            	'username'=> 'required|unique:users|max:255',
            	'password'=> 'required|min:6'
        	]);
    	  	$user = new User;
        	$user->name=$postData['name'];
        	$user->email=$postData['email'];
        	$user->phone_no=$postData['phone'];
        	$user->username=$postData['username'];
        	$user->password=Hash::make($postData['password']);
            $user->created_by=Auth::user()->id;
            $user->status=1;
            $user->created_at=date('Y-m-d H:i:s');
            $user->updated_at=NULL;
            if($user->save()){
            	return redirect('/admin/dashboard')->withSuccess('You user created successfully!');
            }else{
            	return redirect('/admin/dashboard')->withErrors('Something error try after some time!');
            }
		}
	}

	function check_unique_username($id=null, Request $request){        
		$respose['status']=1;
		if($request->ajax() && $request->isMethod('post')){
			$postData = $request->all();
			if(!empty($postData['username'])){
				$username=$postData['username'];
				if(!empty($postData['id'])){
					$userCount  = User::where('username',$username)->where('id','!=',$postData['id'])->count();
				}else{
					$userCount  = User::where('username',$username)->count();
				}
				if(!empty($userCount)){
					$respose['status']=1;
				}else{
					$respose['status']=0;
				}

			}
		}       
		return Response::json($respose);
	}

	function check_unique_email($id=null, Request $request){
		$respose['status']=1;
		if($request->ajax() && $request->isMethod('post')){
			$postData = $request->all();
			if(!empty($postData['email'])){
				$email=$postData['email'];
				if(!empty($postData['id'])){
					$userCount  = User::where('email',$email)->where('id','!=',$postData['id'])->count();
				}else{
					$userCount  = User::where('email',$email)->count();
				}

				if(!empty($userCount)){
					$respose['status']=1;
				}else{
					$respose['status']=0;
				}

			}
		}
		return Response::json($respose);
	}

	function ajax_change_status(Request $request){
		$respose['status']=1;
		if($request->ajax() && $request->isMethod('post')){
			$postData = $request->all();
			$status = $postData['status'];
			$id = $postData['id'];
			User::where('id', $id)->update(['status' => $status]);
			if(!empty($status)){
				$respose['message']='User has been actived successfully.';
			}else{
				$respose['message']='User has been inactived successfully.';
			}

		}
		return Response::json($respose);

	}

	function ajax_view(Request $request){
		if($request->ajax() && $request->isMethod('post')){
			$postData = $request->all();
			$id = $postData['id'];
			$this->viewVars['user'] = User::find($id);
		}
		//dd($this->viewVars);
		return view('admin.users.ajax.view',$this->viewVars);
		// echo 1; exit;
	}

	function ajax_edit(Request $request){
		if($request->ajax() && $request->isMethod('post')){
			$postData = $request->all();
			$id = $postData['id'];
			$this->viewVars['user'] = User::find($id);
		}
		//$this->viewVars['roles'] = Role::where('status',1)->where('deleted_flag',0)->get()->pluck('role_name','id')->toArray();
		$this->viewVars['ra_users'] = User::where('status',1)->get()->pluck('name','id')->toArray();
		return view('admin.users.ajax.edit',$this->viewVars);
	}

	function update($id,Request $request){
		if($request->isMethod('post')){
			$postData = $request->all();           
			$request->validate([
	            'name'=> 'required',            	
            	'email'=> 'required|max:255|unique:users,email,'.$id,
            	'phone'=> 'required|min:10|max:10|unique:users,phone_no,'.$id,
            	'username'=> 'required|max:255|unique:users,username,'.$id
        	]);
        	
            //print_r(User::all());exit;
            $user = User::find($id);
        	$user->name=$postData['name'];        	
        	$user->email=$postData['email'];
        	$user->phone_no=$postData['phone'];        
        	$user->username=$postData['username'];
            $user->updated_by=Auth::user()->id;
            $user->status=1;
            $user->updated_at=date('Y-m-d H:i:s');;
            if($user->save()){
            	return redirect('/admin/dashboard')->withSuccess('You user updated successfully!');
            }else{
            	return redirect('/admin/dashboard')->withErrors('Something error try after some time!');
            }
		}
	}

	function delete($id){
        User::where('id',$id)->update(['deleted_flag'=>1,'updated_at'=>now()]);
       	return redirect('admin/dashboard')->withSuccess('You have successfully delete user!');
	}

	function change_password($id){
		$user = User::find($id);
    	$user->password=Hash::make('Admin@123');
        $user->updated_by=Auth::user()->id;
        $user->status=1;
        $user->updated_at=date('Y-m-d H:i:s');;
        if($user->save()){
        	return redirect('admin/dashboard')->withSuccess('You have successfullyc change password!');
        }else{
        	return redirect('admin/dashboard')->withErrors('Something error try after some time!');
        }
	}
}
