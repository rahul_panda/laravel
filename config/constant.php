<?php 
defined('SUPER_ADMIN')       || define('SUPER_ADMIN', 1); 
defined('ADMIN')             || define('ADMIN', 2); 
defined('EMPLOYEE')          || define('EMPLOYEE', 3); 

defined('USER_TYPE')         || define('USER_TYPE', json_encode([SUPER_ADMIN=>'Super Admin',ADMIN=>'Admin',EMPLOYEE=>'Employee'])); 

defined('PAGE_LIMIT')       || define('PAGE_LIMIT', 10);