<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Eco Tour India - Login</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{url('assets/img/favicon.png')}}">

		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="{{url('assets/css/feather.css')}}">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
		
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		
			<div class="header d-none">
				<!-- Header Menu -->
				<ul class="nav nav-tabs user-menu">
					<!-- Flag -->
					<li class="nav-item">
						<a href="#" id="dark-mode-toggle" class="dark-mode-toggle">
							<i class="feather-sun light-mode"></i><i class="feather-moon dark-mode"></i>
						</a>
					</li>
					<!-- /Flag -->
				</ul>
				<!-- Header Menu -->
			</div>
			
			<div class="row">
			
				<!-- Login Banner -->
				<div class="col-md-6 login-bg">
					<div class="login-banner"></div>
				</div>
				<!-- /Login Banner -->
				
				<div class="col-md-6 login-wrap-bg">		
				
					<!-- Login -->
					<div class="login-wrapper">
						<div class="loginbox">
							<div class="img-logo text-center">
								<img src="{{url('assets/img/logo.svg')}}" class="img-fluid" alt="Logo" style="height: 150px;width: 300px;">
							</div>
							<h3>Login</h3>
							<p class="account-subtitle">login to your account to continue</p>
										
							<form action="{{url('admin/login')}}"  method="post" id="loginForm">
								@csrf
								<div class="form-group form-focus">
									<input type="text" class="form-control floating" name="username" id="username">
									<label class="focus-label">Enter Username</label>
								</div>
								<div class="form-group form-focus">
									<input type="password" class="form-control floating" name="password" id="password">
									<label class="focus-label">Enter Password</label>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-6">
											<label class="custom_check mr-2 mb-0 d-inline-flex"> Remember me  
												<input type="checkbox" name="radio">
												<span class="checkmark"></span>
											</label>
										</div>
										<div class="col-6 text-end">
											<a class="text-success" href="{{url('forgot_password')}}">Forgot Password ?</a>
										</div>
									</div>
								</div>
								<div class="d-grid">
								<button class="btn btn-success" type="submit">Login</button>
								</div>
								
							</form>
						</div>
					</div>
					<!-- /Login -->
					
				</div>
				
			</div>
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="{{url('assets/js/jquery-3.6.0.min.js')}}"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="{{url('assets/js/bootstrap.bundle.min.js')}}"></script>
		
		<!-- Custom JS -->
		<script src="{{url('assets/js/script.js')}}"></script>
		<script src="{{url('assets/js/jquery.validate.min.js')}}"></script>
		<script type="text/javascript">
			$(function(){
				$("#loginForm").validate({
					rules: {
						username: {
							required: true
						},
						password: {
							required: true
						}
					},
					messages: {
						username: "Please enter a valid username",
						password:"Please enter a valid password",
					},
					errorElement: "em",
					highlight: function (element, errorClass, validClass) {
						var elem = $(element);
						if (elem.hasClass("select2-hidden-accessible")) {
							$("#select2-" + elem.attr("id") + "-container").parent().addClass( "is-invalid" ).removeClass( "is-valid" );
						} else {
							$( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
						}
					},
					unhighlight: function (element, errorClass, validClass) {
						var elem = $(element);
						if (elem.hasClass("select2-hidden-accessible")) {
							$("#select2-" + elem.attr("id") + "-container").parent().addClass( "is-valid" ).removeClass( "is-invalid" );
						} else {
							$( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
						}
					},
					errorPlacement: function(error, element) {
						var elem = $(element);
						if (elem.hasClass("select2-hidden-accessible")) {
							element = $("#select2-" + elem.attr("id") + "-container").parent();
							error.addClass( "invalid-feedback" );
				       		// error.insertAfter(element.next(".pmd-textfield-focused"));
				       		error.insertAfter(element);
				       	} else {
				       		error.addClass( "invalid-feedback" );
				       		error.insertAfter(element);
				       		// error.insertAfter(element.next(".pmd-textfield-focused"));
				       	}
				       },
				});
			});
		</script>
    </body>
</html>