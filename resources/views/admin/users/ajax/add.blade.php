@php $userTypeArr = json_decode(USER_TYPE,true); @endphp
<div class="modal-dialog modal-dialog-centered modal-lg">
	<div class="modal-content doctor-profile">
		<div class="modal-header">
			<h3 class="mb-0">Add User</h3>
			<button type="button" class="close-btn" data-bs-dismiss="modal" aria-label="Close"><i class="feather-x-circle"></i></button>
		</div>
		<div class="modal-body">
			<form action="{{url('admin/users/save')}}"  method="post" id="addUserForm">
				@csrf
				<div class="add-wrap">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group form-focus ">
								<input type="text" class="form-control floating" value="" id="name" name="name" maxlength="125">
								<label class="focus-label">Name <span class="text-danger">*</span></label>
							</div>
						</div>
					</div>

					{{--
					<div class="row">
						<div class="col-md-6">
							<div class="form-group ">
								<select class="select" name="user_type_id" id="user_type_id">
									<option value="">Select User Type</option>
								 	@foreach ($userTypeArr as $key => $value)
				                        <option value="{{$key}}">{{$value}}</option>
				                  	@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<select class="select" name="role_id" id="role_id">
									<option value="">Select Designation</option>
									@foreach ($roles as $key => $value)
				                        <option value="{{$key}}">{{$value}}</option>
				                  	@endforeach
								</select>
							</div>
						</div>
					</div>

					--}}
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-focus">
								<input type="text" class="form-control floating" value="" id="email" name="email" maxlength="125">
								<label class="focus-label">Email <span class="text-danger">*</span></label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-focus">
								<input type="text" class="form-control floating" value="" id="phone" name="phone" maxlength="10" minlength="10">
								<label class="focus-label">Mobile No. <span class="text-danger">*</span></label>
							</div>
						</div>
					</div>
					<div class="row">
					{{--
						<div class="col-md-6">
							<div class="form-group">
								<select class="select" name="reporting_id" id="reporting_id">
									<option value="">Select Reporting Authority</option>
									@foreach ($ra_users as $key => $value)
				                        <option value="{{$key}}">{{$value}}</option>
				                  	@endforeach
								</select>
							</div>
						</div>
						--}}
						<div class="col-md-6">
							<div class="form-group form-focus">
								<input type="text" class="form-control floating" value="" id="username" name="username" maxlength="125">
								<label class="focus-label">Username <span class="text-danger">*</span></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-focus">
								<input type="password" class="form-control floating" value="" id="password" name="password" maxlength="125">
								<label class="focus-label">Password <span class="text-danger">*</span></label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-focus">
								<input type="password" class="form-control floating" value="" id="confirm_password" name="confirm_password" maxlength="125">
								<label class="focus-label">Confirm Password <span class="text-danger">*</span></label>
							</div>
						</div>
					</div>
					<div class="submit-section">
						<button type="submit" class="btn btn-success btn-save">Submit</button>
					</div>								
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.select').select2({
			minimumResultsForSearch: -1,
			width: '100%'
		});
	});
</script>
<script type="text/javascript">
	$(function(){
		$.validator.addMethod("checkUserEmail", 
              function(value, element) {
                  var result = false;
                  $.ajax({
                      type:"POST",
                      async: false,
                      url: "{{url('admin/users/check_unique_email')}}", 
                      data: {email: value},
                      dataType: 'json',
                      success: function(data) {
                          result = (data.status == 1) ? false : true;
                      }
                  });
                  // return true if username is exist in database
                  return result; 
              }, 
              "This email id is already taken! Try another."
        );
     	$.validator.addMethod("checkUserName", 
              function(value, element) {
                  var result = false;
                  $.ajax({
                      type:"POST",
                      async: false,
                      url: "{{url('admin/users/check_unique_username')}}", 
                      data: {username: value},
                      dataType: 'json',
                      success: function(data) {
                          result = (data.status == 1) ? false : true;
                      }
                  });
                  // return true if username is exist in database
                  return result; 
              }, 
              "This username is already taken! Try another."
        );
		$("#addUserForm").validate({
			rules: {
				name: {
					required:true
				},
				user_type_id: {
					required:true
				},
				role_id:{
					required:true
				},
				email:{
					required:true,
					email: true,
					checkUserEmail:true
				},
				phone:{
					required:true,
				 	digits:true,
	              	minlength:10,
	              	maxlength:10
				},
				reporting_id:{
					required:true
				},
				username:{
					required:true,
					minlength:3,
					checkUserName:true
				},
				password: {
					required:true, 
					minlength:6
				},
				confirm_password:  {
					required:true,
					equalTo: "#password", 
					minlength:6
				},
			},
			messages: {
				name: "Please enter valid name",
				user_type_id:"Please select user type",
				role_id:"Please select designation",
				email: {
					required:"Please enter valid email id",
					email:"Please enter valid email id"
				},
				phone: {
					required:"Please enter valid mobile no."
				},
				reporting_id:"Please select reporting authority",
				username: {
					required:"Please enter valid username"
				},
				password: {
					required:"Please enter password"
				},
				confirm_password:{
					required:"Please enter confirm password"
				},
			},
			errorElement: "em",
			highlight: function (element, errorClass, validClass) {
				var elem = $(element);
				// console.log(elem);
				if (elem.hasClass("select2-hidden-accessible")) {
					$("#select2-" + elem.attr("id") + "-container").parent().addClass( "is-invalid" ).removeClass( "is-valid" );
				} else {
					$( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
				}
			},
			unhighlight: function (element, errorClass, validClass) {
				var elem = $(element);
				if (elem.hasClass("select2-hidden-accessible")) {
					$("#select2-" + elem.attr("id") + "-container").parent().addClass( "is-valid" ).removeClass( "is-invalid" );
				} else {
					$( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
				}
			},
			errorPlacement: function(error, element) {
				var elem = $(element);
				if (elem.hasClass("select2-hidden-accessible")) {
					element = $("#select2-" + elem.attr("id") + "-container").parent();
					error.addClass( "invalid-feedback" );
		       		// error.insertAfter(element.next(".pmd-textfield-focused"));
		       		error.insertAfter(element);
		       	} else {
		       		error.addClass( "invalid-feedback" );
		       		error.insertAfter(element);
		       		// error.insertAfter(element.next(".pmd-textfield-focused"));
		       	}
		       },
		});
	});
</script>
	