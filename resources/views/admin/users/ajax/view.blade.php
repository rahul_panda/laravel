@php $userTypeArr = json_decode(USER_TYPE,true); @endphp
<div class="modal-dialog modal-dialog-centered modal-lg">
	<div class="modal-content doctor-profile">
		<div class="modal-header justify-content-center border-bottom-0">
			<h4 class="modal-title">User Details</h4>
			<button type="button" class="close-btn pos-top" data-bs-dismiss="modal" aria-label="Close"><i class="feather-x-circle"></i></button>
		</div>
		<div class="modal-body">
			<div class="member-wrapper">
				<div class="row">
					<div class="col-sm-4">
						<div class="mem-info">
							<h6>Name</h6>
							<p>{{$user->name}}</p>
						</div>
					</div>
					
					
					<div class="col-sm-4">
						<div class="mem-info">
							<h6>Email ID</h6>
							<p>{{$user->email}}</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="mem-info">
							<h6>Mobile No.</h6>
							<p>{{$user->phone_no}}</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="mem-info">
							<h6>Username</h6>
							<p>{{$user->username}}</p>
						</div>
					</div>
				</div>                            
			</div>
			
		</div>
	</div>
</div>