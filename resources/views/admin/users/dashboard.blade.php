@extends('admin.layouts.admin')
@section('page-content')
@php $userTypeArr = json_decode(USER_TYPE,true); @endphp
<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">		
		<!-- Page Header -->
		<div class="page-header">
			<div class="row align-items-center">
				<div class="col-md-12 d-flex justify-content-end">
					<!-- <div class="doc-badge me-3">Total Users <span class="ms-1">48</span></div> -->
					<div class="doc-badge me-3">
						<a href="javascript:void(0);" class="btn btn-success" onclick="addUser();"><i class="feather-user-plus me-1"></i>Add User</a>
					</div>
					<div class="SortBy">
						<div class="selectBoxes order-by">
							<p class="mb-0"><img src="{{url('assets/img/icon/sort.png')}}" class="me-2" alt="icon"> Order by </p>
							<span class="down-icon"><i class="feather-chevron-down"></i></span>
						</div>						  
						<div id="checkBox">
							<form action="patient-list.html">
								<p class="lab-title">Specialities</p>
								<label class="custom_radio w-100">
									<input type="radio" name="year">
									<span class="checkmark"></span> Number of Appointment
								</label>
								<label class="custom_radio w-100">
									<input type="radio" name="year">
									<span class="checkmark"></span> Total Income
								</label>
								<label class="custom_radio w-100 mb-4">
									<input type="radio" name="year">
									<span class="checkmark"></span> Ratings
								</label>
								<p class="lab-title">Sort By</p>
								<label class="custom_radio w-100">
									<input type="radio" name="sort">
									<span class="checkmark"></span> Ascending
								</label>
								<label class="custom_radio w-100 mb-4">
									<input type="radio" name="sort">
									<span class="checkmark"></span> Descending
								</label>
								<button type="submit" class="btn w-100 btn-primary">Apply</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->
		
		<!-- Patients List -->
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<div class="row align-items-center">
							<div class="col">
								<h5 class="card-title">{{$title}}</h5>
							</div>
							<div class="col-auto custom-list d-flex">
								<div class="form-custom me-2">
									<div id="tableSearch"  class="dataTables_wrapper"></div>
								</div>
								<div class="multipleSelection">
									<div class="selectBox">
										<p class="mb-0"><i class="feather-filter me-1"></i> Filter </p>
										<span class="down-icon"><i class="feather-chevron-down"></i></span>
									</div>						  
									<div id="checkBoxes">
										<form action="patient-list.html">
											<p class="lab-title">By Account status</p>
											<div class="selectBox-cont">
												<label class="custom_check w-100">
													<input type="checkbox" name="acc" checked>
													<span class="checkmark"></span> Enabled
												</label>
												<label class="custom_check w-100">
													<input type="checkbox" name="acc">
													<span class="checkmark"></span> Disabled
												</label>
												<p class="lab-title">By Blood Type</p>
												<label class="custom_check w-100">
													<input type="checkbox" name="year">
													<span class="checkmark"></span> AB+
												</label>
												<label class="custom_check w-100">
													<input type="checkbox" name="year">
													<span class="checkmark"></span> O-
												</label>
												<label class="custom_check w-100">
													<input type="checkbox" name="year">
													<span class="checkmark"></span> B-
												</label>
												<label class="custom_check w-100">
													<input type="checkbox" name="year">
													<span class="checkmark"></span> A+
												</label>
												<label class="custom_check w-100 mb-4">
													<input type="checkbox" name="year">
													<span class="checkmark"></span> B+
												</label>
											</div>
											<button type="submit" class="btn w-100 btn-primary">Apply</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body p-0">				
						<div class="table-responsive">									
							<table class="table table-borderless hover-table" >
								<thead class="thead-light">
									<tr>
									   <th>Sl#</th>
									   <th>Name</th>
									   <th>Username</th>
									   <th>Created At</th>
									   <th>Updated At</th>
									   <th>Status</th>
									   <th>Action</th>
									</tr>
								</thead>
								<tbody>
									@php 
										$i = 1;
									@endphp
									@foreach ($users as $key => $value)
									<tr>
										<td>{{$i}}</td>
										<td>
											<span class="user-name">{{$value->name}}</span>
											
											<i class="d-block">{{$value->email}}</i>
											<i class="d-block">{{$value->phone}}</i>
										</td>
										<td>{{$value->username}}</td>
									
										<td>{{!empty($value->created_at)?date('d-m-Y h:i A',strtotime($value->created_at)):'--'}}</td>
										<td>{{!empty($value->updated_at)?date('d-m-Y h:i A',strtotime($value->updated_at)):'--'}}</td>
										<td>
											@if($value->user_type_id != SUPER_ADMIN)
												<label class="toggle-switch" for="status{{$value->id}}">
													<input type="checkbox" class="toggle-switch-input" id="status{{$value->id}}" {{!empty($value->status)?'checked="checked"':''}} onchange="changeStatus({{$value->id}},this);" value="{{$value->status}}">
													<span class="toggle-switch-label" >
														<span class="toggle-switch-indicator"></span>
													</span>
												</label>
											@endif
										</td>
										<td>
											<a href="javascript:void(0);" class="btn btn-sm bg-info-light" onclick="viewUser({{$value->id}});" title="View">
											<i class="far fa-eye"></i> 
											</a>
											@if($value->user_type_id != SUPER_ADMIN)
												<a href="javascript:void(0);" class="btn btn-sm bg-success-light" onclick="editUser({{$value->id}});" title="Edit"><i class="far fa-edit"></i>
												</a>
												<a href="javascript:void(0);" class="btn btn-sm bg-danger-light" onclick="deleteUser({{$value->id}})" title="Delete"><i class="fas fa-trash-alt"></i>
												</a>
												<a href="javascript:void(0);" class="btn btn-sm bg-primary-light" onclick="changePassword({{$value->id}})" title="Change Password"><i class="fas fa-key"></i>
												</a>
											@endif
										</td>
									</tr>
									@php 
										$i++
									@endphp
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
				<div class="row " style="float: right;">
					<div class="col-sm-12">
						{{ $users->onEachSide(5)->links() }}
					</div>
				</div>
			</div>
		</div>
		<!-- /Patient List -->
	</div>
</div>
<!-- /Page Wrapper -->
<!-- Modal -->
<div class="modal fade" id="addUserModal" data-bs-keyboard="false" data-bs-backdrop="static"></div>
<div class="modal fade" id="viewUserModal" data-bs-keyboard="false" data-bs-backdrop="static"></div>
<div class="modal fade" id="editUserModal" data-bs-keyboard="false" data-bs-backdrop="static"></div>
<!-- /Modal -->
<script type="text/javascript">
	function addUser(){
		$.ajax({
			type: "POST",
            url: "{{url('admin/users/ajax_add')}}",
            data: {},
            dataType: 'html',
         	success: function (data) {
         		$("#addUserModal").html(data);
         		$("#addUserModal").modal('show');
         	}
        });
	}
	function changeStatus(id,obj){
		var hid = $(obj).attr('id');
		var status;
		if($(obj).prop('checked')===true){
			status=1;
		}else{
			status=0;
		}
		Swal.fire({
	        title: 'Are you sure?',
	        text: "Do you want to change status ?",
	        icon: 'warning',
	        showCancelButton: true,
	        confirmButtonColor: '#3085d6',
	        cancelButtonColor: '#d33',
	        confirmButtonText: 'Yes'
	        }).then((result) => {
	        if (result.isConfirmed) {
	        	$.ajax({
					type: "POST",
		            url: "{{url('admin/users/ajax_change_status')}}",
		            data: {'status':status,id:id},
		            dataType: 'json',
		         	success: function (data) {
		         		if(data.status==1){
		         			Swal.fire(
							  'Success!',
							   data.message,
							  'success'
							)
		         		}
		         	}
		        });
	        }else{
	        	if($(obj).prop('checked')===true){
	        		$("#"+hid).prop("checked", false)
	        	}else{
	        		$("#"+hid).prop('checked',true);
	        	}
	        }
	    });
	}
	function viewUser(id){
		$.ajax({
			type: "POST",
            url: "{{url('admin/users/ajax_view')}}",
            data: {'id':id},
            dataType: 'html',
         	success: function (data) {
         		$("#viewUserModal").html(data);
         		$("#viewUserModal").modal('show');
         	}
        });
	}

	function editUser(id){
		$.ajax({
			type: "POST",
            url: "{{url('admin/users/ajax_edit')}}",
            data: {'id':id},
            dataType: 'html',
         	success: function (data) {
         		$("#editUserModal").html(data);
         		$("#editUserModal").modal('show');
         	}
        });
	}
	function deleteUser(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                window.location.href="{{url('admin/users/delete')}}/"+id;
            }
        })
   }
   function changePassword(id){
   		Swal.fire({
            title: 'Change Password',
            text: "Do you want to change the password (Admin@123)",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, change it!'
            }).then((result) => {
            if (result.isConfirmed) {
                window.location.href="{{url('admin/users/change_password')}}/"+id;
            }
        })
   }
</script>
@endsection