<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<title>Eco Tour India</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Favicon -->
		<link rel="shortcut icon" href="{{url('assets/img/favicon.png')}}">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">

		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="{{url('assets/plugins/fontawesome/css/fontawesome.min.css')}}">
		<link rel="stylesheet" href="{{url('assets/plugins/fontawesome/css/all.min.css')}}">

		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="{{url('assets/css/feather.css')}}">

		<!-- Select2 CSS-->
        <link rel="stylesheet" href="{{url('assets/plugins/select2/css/select2.min.css')}}">

		<!-- Owl carousel CSS -->
		<link rel="stylesheet" href="{{url('assets/plugins/owl-carousel/owl.carousel.min.css')}}">

		<!-- Daterangepicker CSS -->
		<link rel="stylesheet" href="{{url('assets/plugins/daterangepicker/daterangepicker.css')}}">

        <link rel="stylesheet" href="{{url('assets/plugins/datatables/datatables.min.css')}}">
		<!-- Main CSS -->
		<link rel="stylesheet" href="{{url('assets/css/style.css')}}">
        <link rel="stylesheet" href="{{url('assets/css/sweetalert2.min.css')}}">
        <!-- jQuery -->
		<script src="{{url('assets/js/jquery-3.6.0.min.js')}}"></script>
        {{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"> --}}
	</head>
	<body>
		<script type="text/javascript">
			$(function(){
				$.ajaxSetup({
		            headers: {
		                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
		            }
	    		});
			});
		</script>
		<!-- Main Wrapper -->
		<div class="main-wrapper">
			
			@include('includes.flash')
			@include('includes.header')
			@include('includes.leftmenu')
			@yield('page-content')
			@include('includes.footer')
		</div>
		<!-- /Main Wrapper -->
	</body>
</html>
