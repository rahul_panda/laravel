<div style="position: relative;">
	 @if ($message = Session::get('success'))
		<div class="alert alert-success alert-dismissible fade show" role="alert"  style="float: right;position: absolute;z-index: 9999;right: 5px;">
			{{ $message }}
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" fdprocessedid="vffroh"></button>
		</div>
	@elseif($message = Session::get('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert"  style="float: right;position: absolute;z-index: 9999;right: 5px;">
		{{ $message }}
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" fdprocessedid="2408oj"></button>
	</div>
	@endif
	@if ($errors->any())
	    <div class="alert alert-danger alert-dismissible fade show" role="alert"  style="float: right;position: absolute;z-index: 9999;right: 5px;">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" fdprocessedid="2408oj"></button>
	 	</div>
	@endif
	<!-- <div class="alert alert-primary alert-dismissible fade show" role="alert"  style="float: right;position: absolute;z-index: 9999;right: 5px;">
		<strong>Holy guacamole!</strong> You should check in on some of those fields below.
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" fdprocessedid="e2aorb"></button>
	</div>
	<div class="alert alert-secondary alert-dismissible fade show" role="alert"  style="float: right;position: absolute;z-index: 9999;right: 5px;">
		<strong>Holy guacamole!</strong> You should check in on some of those fields below.
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" fdprocessedid="jnf1j"></button>
	</div>
	
	<div class="alert alert-warning alert-dismissible fade show" role="alert"  style="float: right;position: absolute;z-index: 9999;right: 5px;">
		<strong>Holy guacamole!</strong> You should check in on some of those fields below.
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" fdprocessedid="szopz8"></button>
	</div> -->
</div>