@if(!empty($breadcrumb))
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb mb-0">
			@foreach ($breadcrumb as $bkey => $bvalue)
				@if(!empty($bvalue['URL']))
					<li class="breadcrumb-item"><a href="{{url($bvalue['URL'])}}">{{$bvalue['name']}}</a></li>
				@else
					<li class="breadcrumb-item active" aria-current="page">{{$bvalue['name']}}</li>
				@endif
			@endforeach
		</ol>
	</nav>
@endif