<!-- Sidebar -->
<div class="sidebar" id="sidebar">
	<div class="sidebar-inner slimscroll">
		<div id="sidebar-menu" class="sidebar-menu">
			<ul>
				<li class="menu-title"><span>Main</span></li>
				<li class="active">
					<a href="{{url('admin/dashboard')}}"><i class="feather-grid"></i> <span>Dashboard</span></a>
				</li>

				<li class="submenu">
					<a href="#"><i class="feather-file-text"></i> <span> Master</span> <span class="menu-arrow"></span>
					</a>
					<ul>
						<li><a href="{{url('scheme/index')}}">Scheme</a></li>
						<li><a href="{{url('destination/index')}}">Destination</a></li>
						<li><a href="{{url('country/index')}}">Country</a></li>
						<li><a href="{{url('state/index')}}">State</a></li>
					</ul>
				</li>
				<li class="submenu">
					<a href="#"><i class="feather-users"></i> <span>User Management</span> <span class="menu-arrow"></span>
					</a>
					<ul>
						<li><a href="{{url('users/index')}}">Users</a></li>
					</ul>
				</li>
				<li class="submenu">
					<a href="#"><i class="feather-settings"></i> <span>Setting</span> <span class="menu-arrow"></span>
					</a>
					<ul>
						<li><a href="{{url('setting/appsetting/index')}}">App Setting</a></li>
						<li><a href="{{url('roles/index')}}">Roles</a></li>
						<li><a href="{{url('modules/index')}}">Module</a></li>
						<li><a href="{{url('rnp/index')}}">Roles & Permission</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- /Sidebar -->
