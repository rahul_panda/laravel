<!-- Bootstrap Core JS -->
<script src="{{url('assets/js/bootstrap.bundle.min.js')}}"></script>

<!-- Feather Icon JS -->
<script src="{{url('assets/js/feather.min.js')}}"></script>

<!-- Select2 JS -->
<script src="{{url('assets/plugins/select2/js/select2.min.js')}}"></script>

<!-- Slimscroll JS -->
<script src="{{url('assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Owl Carousel JS -->
<script src="{{url('assets/plugins/owl-carousel/owl.carousel.min.js')}}"></script>

<!-- Chart JS -->
<script src="{{url('assets/plugins/apexchart/apexcharts.min.js')}}"></script>
<script src="{{url('assets/plugins/apexchart/chart-data.js')}}"></script>

<!-- Daterangepicker JS -->
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{url('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables/datatables.min.js')}}"></script>

<!-- Custom JS -->
<script src="{{url('assets/js/script.js')}}"></script>


<script src="{{url('assets/js/jquery.validate.min.js')}}"></script>
<script src="{{url('assets/js/sweetalert2.all.min.js')}}"></script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.33/sweetalert2.min.js" ></script> -->
